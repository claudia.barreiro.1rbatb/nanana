//
//  ContentView.swift
//  nanana
//
//  Created by Claudia Barreiro on 22/07/2020.
//  Copyright © 2020 Claudia Barreiro. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
